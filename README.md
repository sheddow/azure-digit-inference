# Azure Digit Inference
Digit recognition running on Azure Functions. The model used is created with fastai, using both handwritten and computer generated digits (see https://colab.research.google.com/gist/DonSheddow/7c83f6fe6e59cc89149de753ed41c407/digit_recognition.ipynb). Used in [Sudoku Vision](https://gitlab.com/sheddow/sudoku-vision).
