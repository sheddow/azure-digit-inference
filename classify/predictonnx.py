import numpy as np
import onnxruntime
import json
import time

import logging
import os
import sys

from PIL import Image


d = os.path.dirname(os.path.abspath(__file__))
modelfile = os.path.join(d, 'model.onnx')

session = onnxruntime.InferenceSession(modelfile, None)

input_name = session.get_inputs()[0].name  


def squish(inp):
    return inp / 255

def unskew(inp):
    assert inp.ndim == 3
    mean_vec = np.array([0.485, 0.456, 0.406])
    stddev_vec = np.array([0.229, 0.224, 0.225])

    res = (inp.swapaxes(0, 2) - mean_vec)/stddev_vec

    return res.swapaxes(0, 2)


def preprocess(input_data):
    assert input_data.ndim == 2

    input_data = input_data.reshape((1, 28, 28)).repeat(3, axis=0)

    normalized = unskew(squish(input_data))

    vectorized = normalized.reshape((1, 3, 28, 28))

    assert vectorized.shape == (1, 3, 28, 28)

    return vectorized.astype('float32')

def softmax(x):
    x = x.reshape(-1)
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)

def postprocess(result):
    return softmax(np.array(result)).tolist()

def predict_images(images):
    if len(images) > 81:
        raise ValueError("too many images")
    if len(images) == 0:
        return []

    arrays = []
    for im in images:
        arrays.append(preprocess(im))

    input_data = np.concatenate(arrays)

    if len(arrays) < 81:
        padding = np.array(arrays[-1]).repeat(81 - len(arrays), axis=0)
        input_data = np.concatenate((input_data, padding))

    start = time.time()
    raw_result = session.run([], {input_name: input_data})
    end = time.time()

    inference_time = np.round((end - start) * 1000, 2)
    logging.warning(f'inference time: {inference_time}')

    results = []
    for i in range(len(images)):
        res = postprocess(raw_result[0][i])
        idx = np.argmax(res)
        r = {
            'prediction': int(idx),
            'confidence': res[idx]
        }
        results.append(r)

    return results


def predict_images_from_bytes(list_of_bytes):
    images = [Image.frombytes('L', (28, 28), b, 'raw') for b in list_of_bytes]
    return predict_images([np.array(i) for i in images])


def predict_image_from_path(path):
    image = Image.open(path)
    return predict_images([np.array(image)])[0]


if __name__ == '__main__':
    print(predict_image_from_path(sys.argv[1]))
