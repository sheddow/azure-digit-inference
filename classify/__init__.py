import base64
import json
import logging

import azure.functions as func

from .predictonnx import predict_images_from_bytes


def main(req: func.HttpRequest) -> func.HttpResponse:
    body = req.get_json()

    results = predict_images_from_bytes(map(base64.b64decode, body['images']))

    headers = {
        "Content-type": "application/json",
        "Access-Control-Allow-Origin": "*"
    }

    logging.warn(json.dumps(results, indent=4));

    return func.HttpResponse(json.dumps(results, indent=4), headers=headers)
